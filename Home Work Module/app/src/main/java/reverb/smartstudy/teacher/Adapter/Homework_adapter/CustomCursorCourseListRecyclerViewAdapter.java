package reverb.smartstudy.teacher.Adapter.Homework_adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mdjahirulislam.youtubestyletabs.R;

import reverb.smartstudy.teacher.Adapter.CursorRecyclerViewAdapter;
import reverb.smartstudy.teacher.ui.CustomCourseViewHolder;
import reverb.smartstudy.teacher.ui.CustomViewHolder;

/**
 * Created by Lenovo on 11/20/2017.
 */

public class CustomCursorCourseListRecyclerViewAdapter extends CursorRecyclerViewAdapter{

    public CustomCursorCourseListRecyclerViewAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.courseuint, parent, false);
        return new CustomCourseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {
        CustomCourseViewHolder holder = (CustomCourseViewHolder) viewHolder;
        cursor.moveToPosition(cursor.getPosition());
        holder.setData(cursor);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
}
