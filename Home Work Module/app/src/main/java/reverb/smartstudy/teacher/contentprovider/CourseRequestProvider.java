package reverb.smartstudy.teacher.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import reverb.smartstudy.teacher.database.CourseTableItems;
import reverb.smartstudy.teacher.database.CustomSqliteOpenHelper;

/**
 * Created by Lenovo on 11/25/2017.
 */

public class CourseRequestProvider extends ContentProvider {
    private static final String TAG = "CourseRequestProvider";

    private SQLiteOpenHelper mSqliteOpenHelper;
    private static final UriMatcher sUriMatcher;

    public static final String AUTHORITY = "reverb.smartstudy.courseRequestProvider";

    private static final int
            TABLE_ITEMS = 3;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI("reverb.smartstudy.courseRequestProvider", CourseTableItems.COURSE_TABLE_NAME + "/offset/" + "#", TABLE_ITEMS);
        sUriMatcher.addURI("reverb.smartstudy.courseRequestProvider", "events" + "/offset/" + "#", 1);
    }

    public static Uri urlForItems(int limit) {
        Uri uri= Uri.parse("content://" + "reverb.smartstudy.courseRequestProvider"+ "/" + CourseTableItems.COURSE_TABLE_NAME + "/offset/" + limit);
        return Uri.parse("content://" + "reverb.smartstudy.courseRequestProvider"+ "/" + CourseTableItems.COURSE_TABLE_NAME + "/offset/" + limit);
    }

    public static Uri urlCourseTable() {
        return Uri.parse("content://" +"reverb.smartstudy.courseRequestProvider" + "/" + CourseTableItems.COURSE_TABLE_NAME);
    }

    public static Uri urlForCourse(int limit) {
        return Uri.parse("content://" + "reverb.smartstudy.courseRequestProvider" + "/" + "course" + "/offset/" + limit);
    }




    @Override
    public boolean onCreate() {
        mSqliteOpenHelper = new CustomSqliteOpenHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mSqliteOpenHelper.getReadableDatabase();
        SQLiteQueryBuilder sqb = new SQLiteQueryBuilder();
        Cursor c = null;
        String offset;

        int i=sUriMatcher.match(uri);

        Log.d(TAG, "query: "+i+"    ---  uri----> "+uri);
        switch (sUriMatcher.match(uri)) {
            case TABLE_ITEMS: {
                sqb.setTables(CourseTableItems.COURSE_TABLE_NAME);
                offset = uri.getLastPathSegment();
                break;
            }

            default:
                throw new IllegalArgumentException("uri not recognized!");
        }

        int intOffset = Integer.parseInt(offset);

        String limitArg = intOffset + ", " + 30;
        Log.d(TAG, "query: " + limitArg);
        c = sqb.query(db, projection, selection, selectionArgs, null, null, sortOrder, limitArg);

        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return AUTHORITY + ".item";
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        String table = "";
        int i=sUriMatcher.match(uri);
        switch (sUriMatcher.match(uri)) {
            case TABLE_ITEMS: {
                Log.d(TAG, "insert: Enter switch case");
                table = CourseTableItems.COURSE_TABLE_NAME;
                break;
            }

            case 1: {
                table = "events";
                break;
            }
        }

        long result = mSqliteOpenHelper.getWritableDatabase().insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        Log.e(TAG, "insert: result "+ result);

        if (result == -1) {
            throw new SQLException("insert with conflict!");
        }

        Uri retUri = ContentUris.withAppendedId(uri, result);
        return retUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String table = CourseTableItems.COURSE_TABLE_NAME;
        if(selection!=null){
            int result = mSqliteOpenHelper.getWritableDatabase().delete(table, selection,selectionArgs);
            return result;
        }


        //  int result = mSqliteOpenHelper.getWritableDatabase().delete(table, selection,selectionArgs);
        return -1;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return -1;
    }
}
