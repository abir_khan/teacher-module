package reverb.smartstudy.teacher.Activity.home_work;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.mdjahirulislam.youtubestyletabs.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import reverb.smartstudy.teacher.Activity.news.NewsActivity;
import reverb.smartstudy.teacher.Adapter.CustomCursorRecyclerViewAdapter;
import reverb.smartstudy.teacher.Adapter.Homework_adapter.CustomCursorCourseListRecyclerViewAdapter;
import reverb.smartstudy.teacher.contentprovider.CourseRequestProvider;
import reverb.smartstudy.teacher.contentprovider.RequestProvider;
import reverb.smartstudy.teacher.database.CourseTableItems;
import reverb.smartstudy.teacher.database.NewsTableItems;
import reverb.smartstudy.teacher.interfaces.CourseInterface;
import reverb.smartstudy.teacher.interfaces.NewsInterface;
import reverb.smartstudy.teacher.model.Course;
import reverb.smartstudy.teacher.model.CourseRequest;
import reverb.smartstudy.teacher.model.News;
import reverb.smartstudy.teacher.model.NewsRequest;
import reverb.smartstudy.teacher.preference.SharedPref;
import reverb.smartstudy.teacher.staticclasses.Functions;

public class Home_workActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public final int offset = 30;
    private int page = 0;

    private RecyclerView mRecyclerView;
    private boolean loadingMore = false;
    private Toast shortToast;
    CustomCursorCourseListRecyclerViewAdapter mAdapter;
    Context c1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c1 = this;
        setContentView(R.layout.activity_home_work);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new CustomCursorCourseListRecyclerViewAdapter(this, null);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        fetchCourse();

        shortToast = Toast.makeText(this, "Course List", Toast.LENGTH_SHORT);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                int maxPositions = layoutManager.getItemCount();

                if (lastVisibleItemPosition == maxPositions - 1) { //self comment
                    //  if (lastVisibleItemPosition == maxPositions - 10) {
                    if (loadingMore)
                        return;

                    loadingMore = true;
                    page++;
                    getSupportLoaderManager().restartLoader(0, null, Home_workActivity.this);
                }
            }
        });

    }

    private void fillTestElements() {
        int size = 150;
        ContentValues[] cvArray = new ContentValues[size];
        for (int i = 0; i < cvArray.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(CourseTableItems.NAME, ("Course Name " + i));
            cv.put(CourseTableItems.DESCRIPTION, ("Course Description " + i));
            cv.put(CourseTableItems.CREATED_AT, ("2017-07-18 08:53:44"));
            cv.put(CourseTableItems.UPDATED_AT, ("2017-07-18 08:53:44"));
            cv.put(CourseTableItems.IMAGE, ("default.jpg"));
            cvArray[i] = cv;
        }
    }

    public void fillDBWithDummyData(){
        ContentValues cv = new ContentValues();
        cv.put(CourseTableItems.NAME, ("Course Name" ));
        cv.put(CourseTableItems.DESCRIPTION, ("Course Description "));
        cv.put(CourseTableItems.CREATED_AT,("2016-02-15 03:04:30"));
        cv.put(CourseTableItems.UPDATED_AT,("2016-02-16 05:04:30"));
        cv.put(CourseTableItems.IMAGE,("default.jpg"));
        getContentResolver().insert(RequestProvider.urlForItems (0), cv);
    }

    public void fetchCourse(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://smartstudy.com.bd/demo/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CourseInterface service =  retrofit.create(CourseInterface.class);
        CourseRequest newsRequest = new CourseRequest();
        newsRequest.setUsername(SharedPref.getInstance(getApplicationContext()).getUsername());
        newsRequest.setPassword(SharedPref.getInstance(getApplicationContext()).getPassword());

        Call<Course> courseRespionseCall = service.getCourse(newsRequest);
        courseRespionseCall.enqueue(new Callback<Course>() {
            @Override
            public void onResponse(Call<Course> call, Response<Course> response) {
                int statusCode=response.code();
                if(statusCode == 200){
                    Course course = response.body();

                    if (!course.getError()){
                        ArrayList<Course.Datum> courseList = (ArrayList<Course.Datum>) course.getData();
                        Log.d("TestActivity", String.valueOf(statusCode));
                        Uri a=CourseRequestProvider.urlCourseTable();
                        int count=getContentResolver().delete(CourseRequestProvider.urlCourseTable(), "1",null);
                        for (int i = 0; i < courseList.size(); i++) {

                            ContentValues cv = new ContentValues();
                            cv.put(CourseTableItems.NAME, (courseList.get(i).getName()));
                            cv.put(CourseTableItems.DESCRIPTION, (String) courseList.get(i).getDescription());
                            cv.put(CourseTableItems.CREATED_AT,(Functions.convertTimeStamp(courseList.get(i).getCreatedAt())));
                            cv.put(CourseTableItems.UPDATED_AT,(courseList.get(i).getUpdatedAt()));
                            cv.put(CourseTableItems.IMAGE,(courseList.get(i).getIconpath()));
                            Log.d(TAG,"URI --> "+CourseRequestProvider.urlForItems(0));
                            getContentResolver().insert(CourseRequestProvider.urlForItems(0), cv);
                            Log.d(TAG,"for loop "+i);
                        }
                        getSupportLoaderManager().restartLoader(0, null, Home_workActivity.this);

                    }else {
                        Toast.makeText(c1, "Error Found", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onResponse: error msg"+course.getError() );
                    }
                }else {
                    Log.d(TAG,"Status code error "+statusCode);
                }



            }

            @Override
            public void onFailure(Call<Course> call, Throwable t) {
                Log.d("NewsActivity","On Response: Failed");
                getSupportLoaderManager().restartLoader(0, null, Home_workActivity.this);

            }


        });
    }

/*

    private int getItemsCountLocal() {
        int itemsCount = 0;

        Cursor query = getContentResolver().query(CourseRequestProvider.urlForItems(0), null, null, null, null);
        if (query != null) {
            itemsCount = query.getCount();
            query.close();
        }
        return itemsCount;
    }
*/


            @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

                switch (id) {
                    case 0:
                        return new CursorLoader(this, CourseRequestProvider.urlForItems(offset * page), null, null, null, null);
                    default:
                        throw new IllegalArgumentException("no id handled!");
                }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case 0:
                Log.d(TAG, "onLoadFinished: loading MORE");
                //shortToast.setText("loading MORE " + page);
                if(page!=0){
                    // shortToast.setText("loading more data");
                    //shortToast.show();
                }

                Cursor cursor = ((CustomCursorCourseListRecyclerViewAdapter) mRecyclerView.getAdapter()).getCursor();

                //fill all exisitng in adapter
                MatrixCursor mx = new MatrixCursor(CourseTableItems.Columns);
                fillMx(cursor, mx);

                //fill with additional result
                fillMx(data, mx);

                ((CustomCursorCourseListRecyclerViewAdapter) mRecyclerView.getAdapter()).swapCursor(mx);


                handlerToWait.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingMore = false;
                    }
                }, 2000);

                break;
            default:
                throw new IllegalArgumentException("no loader id handled!");
        }

    }
    private Handler handlerToWait = new Handler();

    private void fillMx(Cursor data, MatrixCursor mx) {
        if (data == null)
            return;

        data.moveToPosition(-1);
        while (data.moveToNext()) {
            mx.addRow(new Object[]{
                    data.getString(data.getColumnIndex(CourseTableItems._ID)),
                    data.getString(data.getColumnIndex(CourseTableItems.NAME)),
                    data.getString(data.getColumnIndex(CourseTableItems.CREATED_AT)),
                    data.getString(data.getColumnIndex(CourseTableItems.DESCRIPTION)),
                    data.getString(data.getColumnIndex(CourseTableItems.UPDATED_AT)),
            });
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // TODO: 2016-10-13
    }

    //

    private static final String TAG = "Home_workActivity";
}
