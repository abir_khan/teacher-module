package reverb.smartstudy.teacher.interfaces;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import reverb.smartstudy.teacher.model.Course;
import reverb.smartstudy.teacher.model.CourseRequest;

/**
 * Created by Lenovo on 11/25/2017.
 */

public interface CourseInterface {
    @POST("api/teacher/courselist")
    Call<Course> getCourse(@Body CourseRequest courseRequest);
}
