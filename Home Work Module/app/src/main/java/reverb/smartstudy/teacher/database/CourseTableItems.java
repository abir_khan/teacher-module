package reverb.smartstudy.teacher.database;

/**
 * Created by Lenovo on 11/25/2017.
 */

public class CourseTableItems {
    public static final String _ID = "_id";
    public static final String TEXT = "text";
    public static final String COURSE_TABLE_NAME = "course";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String CODE_NAME = "codename";
    public static final String IMAGE = "image";

    public static final String CREATE_TABLE =
            " CREATE TABLE " + COURSE_TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " created_at TEXT, " +
                    " updated_at TEXT, " +
                    " name TEXT, " +
                    " description TEXT, " +
                    " codename TEXT, " +
                    " image TEXT);";

    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + COURSE_TABLE_NAME;
    public static String[] Columns = new String[]{_ID, NAME,CREATED_AT,DESCRIPTION,UPDATED_AT};
}
