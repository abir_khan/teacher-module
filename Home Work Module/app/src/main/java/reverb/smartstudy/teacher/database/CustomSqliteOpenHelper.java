package reverb.smartstudy.teacher.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import reverb.smartstudy.teacher.model.CourseRequest;

public class CustomSqliteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "CustomSqliteOpenHelper";
    public static SQLiteDatabase db;

    public CustomSqliteOpenHelper(Context context) {
        super(context, "newsdb.db", null, 3);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate:");
        db.execSQL(NewsTableItems.CREATE_TABLE);
        db.execSQL(CourseTableItems.CREATE_TABLE);
       this.db=db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade: ");
        db.execSQL(NewsTableItems.DROP_TABLE);
        db.execSQL(CourseTableItems.DROP_TABLE);
        onCreate(db);
    }

    public static SQLiteDatabase createTable(){
       db.execSQL(NewsTableItems.CREATE_TABLE);
        return db;
    }



}