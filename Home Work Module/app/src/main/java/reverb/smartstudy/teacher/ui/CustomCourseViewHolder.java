package reverb.smartstudy.teacher.ui;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.mdjahirulislam.youtubestyletabs.R;

/**
 * Created by Lenovo on 11/25/2017.
 */

public class CustomCourseViewHolder extends RecyclerView.ViewHolder {
    String newsName;
    TextView name, createdDate, updatedDate;
    Context context;

    public CustomCourseViewHolder(final View itemView) {
        super(itemView);
        context=itemView.getContext();
        name = (TextView) itemView.findViewById(R.id.courseName);
        createdDate = (TextView) itemView.findViewById(R.id.startDate);
        updatedDate = (TextView) itemView.findViewById(R.id.endDate);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("hi", "Element " + getPosition() + " clicked. "+newsName);
            }
        });
    }
    public void setData(Cursor c) {

        name.setText(c.getString(c.getColumnIndex("name")));
        createdDate.setText(c.getString(c.getColumnIndex("created_at")));
        updatedDate.setText(c.getString(c.getColumnIndex("updated_at")));
    }
}
